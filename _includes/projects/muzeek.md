We build beautiful technology for the live music industry.

Our mission is to provide an incredibly intuitive, flexible and powerful technology platform that helps make the live music industry as efficient as possible while cultivating, supporting and enhancing your relationships. In the end run, we aim to save venues and promoters a ton of time, help artists and their teams automate opportunities, and pass the savings on to the amazing fans that support you.

<https://www.muzeek.com>

**My Role:** Founder / CTO / Head Developer

**Project (v1) built with:**
* PHP
* Apache
* Linux
* MySQL
* HTML
* JavaScript
* A touch of Angular
* A sprinkle of React

**Project (v2) built with:**
* NodeJS
* Javascript / ES6
* Express
* Docker / EC2
* PostgreSQL
* Redis
* React
* JSX
* Webpack
* Gulp