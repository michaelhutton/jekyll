Shiftly was designed and built from the ground up to take the time and hassle out of rostering for your business. Track staff availability, send automated shift notifications, manage budgets and send confirmations directly from the app. Rosters can be printed, sent by email or checked online via a permalink on your phone or tablet.

<http://shiftly.com> (app has been discontinued)

**My Role:** Founder / Head Developer

**Built with:**
* Javascript
* NodeJS
* MeteorJS
* MongoDB
* Handlebars
* nginx
* Jekyll (marketing site)