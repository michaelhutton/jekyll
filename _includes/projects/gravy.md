A fun mobile site to find local food and drink deals. I mainly built this to test the newly released [Meteor][meteor-site] framework. This app used FourSquare data for venue names and locations, and had user-driven content to create and update deals.

<http://gravy.me/> (No longer exists)

**Built with:**
* MeteorJS
* SQLite
* Javascript

[meteor-site]:https://www.meteor.com/