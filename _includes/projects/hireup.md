Hireup was built to change the way that people with disability find, hire and manage their support workers. We have employed, trained and verified over 10,000 support workers to ensure you receive the best support possible.

<https://www.hireup.com.au>

**My Role:** Lead Software Engineer

**Built with:**
* NodeJS
* Javascript / ES6
* Typescript
* Express
* HTML
* Angular
* React
* VueJS 
* (that's right; Angular, React _and_ VueJS)
* Docker
* MongoDB
* GraphQL