connect.com.au was a cloud management platform providing
Virtual Machiness, Cloud storage and CDN for small to medium businesses. The platform was completely API-driven, and built from scratch using NodeJS, Express, MongoDB and Ember.js.

Announced and released the platform at Web Directions South conference in October 2012, 6 months after beginning
the project. Continued development on the platform until AAPT was acquired by TPG Telecom.

<http://www.connect.com.au/> (now redirects to TPG)

**My Role:** Web Applications Developer

**Web App built with:**
* NodeJS
* Express
* CoffeeScript
* MongoDB
* Redis
* EmberJS
* Grunt

**Mobile App built with:**
* Cordova PhoneGap
* JavaScript
* AngularJS
* HAML

**Marketing site built with:**
* Jekyll
* HAML
* Bootstrap