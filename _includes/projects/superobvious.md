Super Obvious is a technology-driven financial services company on a mission to change the way people think about their investments. Since 2020, Super Obvious has had one mission: to help Australians better manage their financial future while also taking care of the planet.

Using cutting-edge technology, Super Obvious empowers our members to better manage their investments – while providing data-driven insights on the ethical impacts their super is having on the country - and the planet.

<https://www.superobvious.com.au>

**My Role:** Lead Developer

**Backend built with:**
* NodeJS
* Typescript
* AWS
* Docker
* PostgreSQL
* GraphQL

**Webapp/www built with:**
* React
* Typescript
* Vercel
* GraphQL
* Storybook

**Mobile app built with:**
* React Native
* Expo
* Typescript
* GraphQL
* Storybook
