---
layout: post
title: "My 1-day website build"
date: 2018-02-24
categories: jekyll website gitlab ci
image: "freerider-skiing-ski-sports-47356.jpg"
image-alt: "Gotta Go Fast!"
---
I wanted my personal website up and running in a day. I wanted it to be simple and fast. I wanted to avoid setting up and managing any type of hosting. I didn't need anything fancy, just a static website that was easy to maintain. The easier it is to maintain, the more often I would make updates!

**My Big 3:**
1. Fast and responsive site.
2. Easy to maintain.
3. Quick turnaround.

**What I was willing to sacrifice:**
1. No underlying database.
2. No fun APIs to build and play around with. :(
3. May not be in my favourite language (JavaScript).

## The Options
A website is a pretty simple thing: some HTML/CSS/JS stored on a server with a public IP address, accessed via a browser. Everything else is an add-on that either complicates or simplifies your life.

I still needed DNS, some SSL certificates, and a few other items, but these would all be the same regardless of implementation. My big questions were:
* _Where would I host the site?_
* _What would I use to build the site?_

So a few quick options I came up with, in approximate increasing order of build-time:
1. Static HTML stored in Dropbox/S3
2. A Wordpress site on EC2/DigitalOcean
3. Static Site Generator (SSG) in Github/GitLab/BitBucket
4. Express NodeJS app stored in AWS EC2
5. API-driven Single Page App stored in EC2 and S3
6. Serverless API-driven Single Page App of awesomeness using Lambda/S3

This was 6 out of about 600 combinations, but it shows you how open building an online app/website/thing can be.

I've used all the above solutions at least once. I also knew that some would be easy to setup but hard to maintain, and others were complete overkill for the task. This didn't need a full DevOps Best Practices guideline, as I was the only one making changes.

## So I went with...
I decided to go with Option 3: **Jekyll** (an SSG) hosted on **GitLab**. 

_Why GitLab?_

GitLab host your static website for free. They also have inbuilt Build Pipelines, allowing you to control the pre-rendering process via commands in a CI config file.

_Why is this awesome?_

Because it means that you can use any SSG and include any plugins your site needs. The Build Runner will then run your config file and build your site on demand. You can customise the build to meet your exact specifications, and not have a black-box build.
Github and Bitbucket definitely didn't have this open build last time I checked, and I couldn't find documentation to the contrary.

_Why Jekyll?_

I also decided to go with [Jekyll][jekyll-site] for my SSG. I did this for convenience (I already had it installed and had used it before) and that it's still popular and relevant. I was very tempted by [Gatsby][gatsby-site] because it's built on JavaScript, React,js and Webpack, avoiding the headache of using ruby and Gemfiles.
I may make the switch to Gatsby, or try out [Hugo][hugo-site], [Next.js][nextjs-site], or any one of the other popular 20 SSGs, but for the time being I'm sticking with Jekyll.

## The Setup
Instead of boring you with the entire process step-by-step, I'll link you to the few resources that I used. I wanted to have a quick deployment, so following a few tutorials for the initial setup seemed a sensible option.

The bulk of the process in setting up your GitLab pages. Marcia Ramos wrote a fantastic article on hosting [GitLab Pages on GitLab.com][gitlab-pages-tutorial]. You can follow it step-by-step and you'll have a great setup, hosted at either https://_yourname_.gitlab.io or at a custom domain if you did the optional step.

I did the option step of setting up a custom domain, so at this point I had my site hosted at http://michaelhutton.com.au. The only issue I had was that I wanted my site hosted via HTTPS, but I currently didn't have an SSL certificate. My domain registrar (GoDaddy) could have sorted this for me, but their SSL certificates were overkill and pricey for what I needed.

So, a quick Google search and, lo and behold, _another_ great article by Marcia Ramos! Titled [Setting up GitLab Pages with CloudFlare Certificates][cloudflare-tutorial]... how could I go wrong?

I followed the article, and 30 minutes later I had a free(!) 15 year SSL certificate from Cloudflare! **15 years!** I also get the benefit of Cloudflare doing some caching in the off-chance that I suffer a DDoS... or I get _really_ popular!

## The Maintenance
The whole process only took me an afternoon, and has left me with a really simple setup. To make changes to my website I:
1. Run the local Jekyll server with:

 `jekyll serve --watch --livereload`
2. Make my changes, with the site updating via LiveReload.
3. Commit my changes to the `master` branch.
4. Push to GitLab
5. GitLab takes over, and triggers the Build Pipeline.
6. A Runner builds my Jekyll instance from scratch, and if everything passes, it deploys live!

[jekyll-site]:https://jekyllrb.com/
[gatsby-site]:https://www.gatsbyjs.org/
[hugo-site]:https://gohugo.io/
[nextjs-site]:https://nextjs.org/
[gitlab-pages-tutorial]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/
[cloudflare-tutorial]:https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/