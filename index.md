---
layout: unlistedpage
title: Hi 👋
permalink: /
---

I'm Michael Hutton, and I'm a tech enthusiast with a passion for driving positive change through innovation. 

Right now I'm a Lead Developer / Engineering Manager at [Super Obvious][superobvious], on a mission to revolutionize how Australians manage their financial future while being environmentally responsible.

Over the years, I've had the privilege of working on exciting projects across fintech, the disability/NDIS space, live music, telecoms, and businesses ranging in all shapes and sizes.

I believe in the power of continuous learning and growth; I'm always seeking to expand my horizons.

I almost exclusively read non-fiction; mostly finance or computer science books.

I mostly work on private repos.

Beyond all my other roles, the one that fills my heart with the most joy is being a father. I cherish the moments spent with my daughter, and she is my greatest source of inspiration and motivation.

I currently live in Sydney, Australia.

If you want more details of my work experience, check out my [LinkedIn][linkedin] and my current and previous [projects][projects].

[superobvious]:https://www.superobvious.com.au
[linkedin]:https://www.linkedin.com/in/m-hutton
[projects]:{{ "/projects" | relative_url }}